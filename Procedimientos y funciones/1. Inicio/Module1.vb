﻿Module Module1

    ' Hola, aquí hay procedimientos y funciones, si no te gustan, vete
    ' Dani llama procedimiento a los fragmentos de código rodeados por Sub-End Sub (Como el Main)
    ' La documentación oficial de Microsoft no los llama así, pero como hay que aprobar, ahora son procedimientos
    ' La diferencia entre Sub y Function (Procedimiento y Función) es que el procedimiento no devuelve nada (no return),
    ' y la función puede devolver algo (sí return)
    ' El Main es un ejemplo de procedimiento, es ejecución de código pero no devuelve nada

    Sub Main()

        ' Mostrar algo en pantalla
        Console.WriteLine("-------------------------- Mostrar algo en pantalla --------------------------")
        Mostrar() ' Los procedimientos se llaman como a las funciones, no hay diferencia
        ' Salida: Espinacas

        ' Parámetros por valor
        Console.WriteLine("-------------------------- Parámetros por valor --------------------------")

        Dim numVal As Integer = 5
        Console.WriteLine("Numero en Main: " + numVal.ToString())
        ParametroPorValor(numVal)
        Console.WriteLine("Numero en Main: " + numVal.ToString())
        ' Salida: Main: 5           ParametroPorValor: 6            Main: 5

        ' Parámetros por referencia
        Console.WriteLine("-------------------------- Parámetros por referencia --------------------------")
        Dim numRef As Integer = 5
        Console.WriteLine("Numero en Main: " + numRef.ToString())
        ParametroPorReferencia(numRef)
        Console.WriteLine("Numero en Main: " + numRef.ToString())
        ' Salida: Main: 5           ParametroPorReferencia: 6            Main: 6

        ' Parámetros opcionales
        Console.WriteLine("-------------------------- Parámetros opcionales --------------------------")
        ' Como no le pasamos nada, empieza a contar de 0
        Contar() ' Salida: 0 1 2 3 4 5 6 7 8 9 10
        Contar(2) ' Salida: 2 3 4 5 6 7 8 9 10

        Repetir(5) ' Repite "Pipas Tijuana" 5 veces
        Repetir(2, "Me he distraído con una mosca") ' Repite "Me he distraído con una mosca" 2 veces
        ' Salida: Te la imaginas guap@, que ya bastante he escrito


        ' ParamArray
        Console.WriteLine("-------------------------- ParamArray --------------------------")
        Sumar(1, 2, 3, 4, 5, 6, 7)
        Sumar(1, 2, 3)
        Sumar(1, 2, 3, 6, 2, 6, 2)
        ' Salida: Pues la suma, chico (y Leire), el procedimiento se llama sumar, que hostias esperas


        Console.ReadKey() ' Para que pare al final, no tiene importancia

    End Sub

    ' Empezamos con procedimientos, que son mas sencillos
    ' Todo lo que se hace en los procedimientos se puede hacer en funciones de la misma forma
    ' Podemos hacer un procedimiento que muestre un texto en pantalla
    Sub Mostrar()
        Console.WriteLine("Espinacas")
    End Sub

    ' Ahora con parámetros
    ' Hay cuatro formas de pasar parámetros, y una extra por si eres imbecil
    ' 1: Por valor
    '   Sólo se pasa el valor, la variable originar no queda afectada
    Sub ParametroPorValor(ByVal num As Integer) ' Recibe un Integer por valor que se llama num

        num += 1
        Console.WriteLine("Numero en ParametroPorValor: " + num.ToString())

    End Sub

    ' 2: Parámetros por referencia
    '   Pasa la referencia a la variable. Si se edita en el procedimiento, se edita la original
    Sub ParametroPorReferencia(ByRef num As Integer) ' Recibe un Integer por referencia que se llama num

        num += 1
        Console.WriteLine("Numero en ParametroPorReferencia: " + num.ToString())

    End Sub

    ' 3: Opcional
    '   Pues eso, opcional
    '   Pero no rayarse, hay normas
    '   Los parámetros opcionales siempre van al final, es decir, primero los normales (obligatorios) y luego opcionales
    '   A los opcionales tambien se les aplica el rollo de por valor y por referencia
    '   A los parámetros opcionales, hay que darles un valor por defecto, por si no se los pasamos
    '   Voy a hacer un procedimiento para aprender a contar hasta 10
    '   Si no le pasamos el parámetro opcional, cuenta de 0 a 10
    '   Si se lo pasamos, empieza desde donde le digamos
    Sub Contar(Optional ByVal inicio As Integer = 0) ' Recibe un Integer opcional por valor, y si no se lo pasamos, es 0

        For index = inicio To 10
            Console.WriteLine(index)
        Next

        Console.WriteLine() ' Para que deje un hueco

    End Sub

    ' Ahora un procedimiento que repita una palabra varias veces
    ' La cantidad de veces que la repita es obligatoria
    ' La palabra que repite no. Por defecto repite Pipas Tuajuana
    Sub Repetir(ByVal cantidad As Integer, Optional ByVal cadena As String = "Pipas Tijuana")

        For index = 1 To cantidad
            Console.WriteLine(cadena)
        Next

        Console.WriteLine() ' Para que deje un hueco

    End Sub

    ' 4: ParamArray
    '   Es mas facil de lo que parece, porfa, no dejes de leer
    '   Te prometo que es facil, es solo para pasar todos los argumentos que quieras
    '   Voy a hacer una función que le pasas todos los números que quieras y los suma
    Sub Sumar(ParamArray numeros As Integer()) ' El tipo del parámetro "numero" es Integer(), un array. En C# sería Integer[]

        ' "numeros" es un array normal, se puede hacer todo lo que quieras con él
        ' En este caso, sumar

        Dim suma As Integer = 0

        For Each num As Integer In numeros
            suma += num
        Next

        Console.WriteLine("Suma: " + suma.ToString())

    End Sub

    ' 5: No pasar parámetros, ya que la ausencia de algo representa una nueva forma de interpretar las cosas
    '   La carencia de algo no es una opción vacía y sin significado, sino otra forma, igual de valida que el resto, de hacer las cosas
    '   El no actuar en un momento concreto cuando alguien lo esperaba no significa haber pasado de ello, solo el haber tomado otra decision
    '   Igual que elegir hacer un procedimiento sin parámetros

    '       Odei Bretón.
    Sub ProcedimientoConFilosofia()
        Console.WriteLine("Cagon todos tus muertos")
    End Sub


End Module
