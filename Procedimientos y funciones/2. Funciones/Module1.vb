﻿Module Module1

    ' Hola
    ' Funciones
    ' En el anterior procedimientos

    ' Las funciones son igual que los procedimientos, pero devuelven cosas
    ' El valor devuelto se especifica al final

    Sub Main()

        ' Función útil
        Console.WriteLine("------------------------- Función útil -----------------------")
        Console.WriteLine(FuncionUtil())


        ' Sumar
        Console.WriteLine("------------------------- Sumar -----------------------")
        Dim suma = Sumar(1, 2, 3, 4, 5, 6, 7, 8, 9)
        Console.WriteLine(suma)

        ' Y nada más, los mismo ejemplos del otro pero con Return
        ' Si estás creativo puedes hacerte los tuyos


        Console.ReadKey() ' Para parar al final

    End Sub


    ' Esta es una funcion muy útil, devuelve 0 siempre
    Function FuncionUtil() As Integer ' Al final de la función, fuera de los paréntesis, se especifica el tipo de devolución
        Return 0
    End Function

    ' El tema de los parámetros funciona igual que en los anteriores, se puede hacer lo mismo
    ' Pero se puede ser creativo con lo del return
    Function Sumar(ParamArray numeros As Integer()) As Integer

        Dim suma As Integer = 0

        For Each num As Integer In numeros
            suma += num
        Next

        Return suma

    End Function

End Module
